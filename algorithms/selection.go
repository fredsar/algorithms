package algorithms

// SelectionSort uses the selection algorithm
func SelectionSort(arrayToSort []int) {
	for i := len(arrayToSort) - 1; i > 0; i-- {
		greatestNumberIndex := 0
		for j := 1; j <= i; j++ {
			if arrayToSort[j] > arrayToSort[greatestNumberIndex] {
				greatestNumberIndex = j
			}
		}
		aux := arrayToSort[greatestNumberIndex]
		arrayToSort[greatestNumberIndex] = arrayToSort[i]
		arrayToSort[i] = aux
	}
}
