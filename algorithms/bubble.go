package algorithms

// BubbleSort takes an array an sort it with bubble method
func BubbleSort(arrayToSort []int) {
	for i := len(arrayToSort); i > 0; i-- {
		for j := 0; j < i-1; j++ {
			if arrayToSort[j] > arrayToSort[j+1] {
				actual := arrayToSort[j]
				arrayToSort[j] = arrayToSort[j+1]
				arrayToSort[j+1] = actual
			}
		}
	}
}
