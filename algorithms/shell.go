package algorithms

// ShellSort uses the shell algorithm
func ShellSort(arrayToSort []int) {
	gap := int(len(arrayToSort) / 2)
	for gap > 0 {
		for i := 0; i < len(arrayToSort); i++ {
			latestIndex := i
			newElement := arrayToSort[i]
			for j := i - gap; j >= 0 && arrayToSort[j] > newElement; j -= gap {
				if arrayToSort[j] > newElement {
					arrayToSort[j+gap] = arrayToSort[j]
					latestIndex = j
				}
			}
			arrayToSort[latestIndex] = newElement
		}
		gap--
	}
}
