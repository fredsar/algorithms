package algorithms

// InsertionSort sorts an array with insertin sort
func InsertionSort(arrayToSort []int) {
	for i := 1; i < len(arrayToSort); i++ {
		latestIndex := i
		newElement := arrayToSort[i]
		for j := i; j > 0 && arrayToSort[j-1] > newElement; j-- {
			if arrayToSort[j-1] > newElement {
				arrayToSort[j] = arrayToSort[j-1]
				latestIndex = j - 1
			}
		}
		arrayToSort[latestIndex] = newElement
	}
}
