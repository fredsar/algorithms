package main

import (
	"fmt"

	"gitlab.com/fredsar/algorithms/algorithms"
)

func main() {
	arrayToSort := []int{20, -15, 35, 7, 55, 1, -22}
	fmt.Println(arrayToSort)
	algorithms.BubbleSort(arrayToSort)
	fmt.Println(arrayToSort)

	arrayToSort = []int{20, -15, 35, 7, 55, 1, -22}
	fmt.Println(arrayToSort)
	algorithms.SelectionSort(arrayToSort)
	fmt.Println(arrayToSort)

	arrayToSort = []int{20, -15, 35, 7, 55, 1, -22}
	fmt.Println(arrayToSort)
	algorithms.InsertionSort(arrayToSort)
	fmt.Println(arrayToSort)

	arrayToSort = []int{20, -15, 35, 7, 55, 1, -22}
	fmt.Println(arrayToSort)
	algorithms.ShellSort(arrayToSort)
	fmt.Println(arrayToSort)
}
